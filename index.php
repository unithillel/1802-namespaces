<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
use HomeProject\UserLib;
use HomeProject\UserLib\DataBase;
use HomeProject\UserLib\DataBase\UserModel as HPUserModel;

//require_once 'HomeProject/SuperClass.php';
//require_once 'HomeProject/MegaClass.php';
//require_once 'namespaces.php';
//require_once 'HomeProject/UserLib/User.php';
//require_once 'HomeProject/UserLib/DataBase/UserModel.php';

class SuperClass
{
    static public function getClassName(){
        return __METHOD__;
    }
}
class UserModel{};

echo SuperClass::getClassName();
echo '<br>';
echo \HomeProject\SuperClass::getClassName();
echo '<br>';
echo \SuperNameSpace\sayHello();
echo '<br>';
echo \AnotherNameSpace\sayHello();
echo '<br>';
echo \HomeProject\MegaClass::megaMethod();
echo '<br>';
echo \HomeProject\UserLib\User::whoAmI();
echo '<br>';
echo \HomeProject\UserLib\DataBase\UserModel::getUserInfo();
echo '<br>';
echo DataBase\UserModel::getUserInfo();
echo '<br>';
echo HPUserModel::getUserInfo();
echo HPUserModel::getUserInfo();
echo HPUserModel::getUserInfo();
$obj = new HPUserModel();
echo '<pre>';
print_r($obj);
die();
